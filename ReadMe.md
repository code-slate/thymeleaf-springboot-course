**Thymeleaf Springboot Course**

All the course video tutorials for the Thymeleaf Springboot Course are on Code Slate YouTube channel.

**How to use the source files**

Each lesson has it's own branch. To see the code for a lesson, select that lesson from the branch drop-down.

